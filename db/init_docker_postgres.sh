#!/bin/bash

# this script is run when the docker container is built
# it imports the base database structure and create the database for the tests

echo "*** CREATING DATABASE ***"

# create default database
cd /docker-entrypoint-initdb.d/scripts

for directory in `ls`
do 
    for sql_file in `ls ${directory}/*.sql`
    do
        echo "RUNNING ${sql_file}"
        # gosu postgres postgres "AGILE_MONKEY_APP" -f ${sql_file}
        psql -d 'AGILE_MONKEY_OWN' -U 'AGILE_MONKEY_OWN' -f ${sql_file}
        # gosu postgres postgres "AGILE_MONKEY_APP" -f ${sql_file}
    done
done

echo "*** END ***"