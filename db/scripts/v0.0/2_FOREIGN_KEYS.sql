ALTER TABLE USR_USER_ROLE ADD CONSTRAINT constraint_usr_user_id_fk FOREIGN KEY (usr_user_id_fk) REFERENCES USR_USER (id);
ALTER TABLE USR_USER_ROLE ADD CONSTRAINT constraint_usr_role_id_fk FOREIGN KEY (usr_role_id_fk) REFERENCES USR_ROLE (id);

ALTER TABLE RES_RESOURCE_IMAGE ADD CONSTRAINT constraint_res_resource_origin_fk FOREIGN KEY (res_resource_origin_fk) REFERENCES RES_RESOURCE_ORIGIN (id);

ALTER TABLE CUS_CUSTOMER ADD CONSTRAINT constraint_profile_res_resource_image_fk FOREIGN KEY (profile_res_resource_image_fk) REFERENCES RES_RESOURCE_IMAGE (id);
ALTER TABLE CUS_CUSTOMER ADD CONSTRAINT constraint_created_by_usr_user_fk FOREIGN KEY (created_by_usr_user_fk) REFERENCES USR_USER (id);
ALTER TABLE CUS_CUSTOMER ADD CONSTRAINT constraint_last_by_usr_user_fk FOREIGN KEY (last_by_usr_user_fk) REFERENCES USR_USER (id);
