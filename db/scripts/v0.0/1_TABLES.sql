CREATE TABLE USR_USER (
	id SERIAL PRIMARY KEY,
	username VARCHAR (50) UNIQUE NOT NULL,
	email VARCHAR (255) UNIQUE NOT NULL,
	password VARCHAR (60) NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE USR_ROLE(
	id INTEGER PRIMARY KEY,
	name VARCHAR(15) NOT NULL
);

CREATE TABLE USR_USER_ROLE (
	usr_user_id_fk INTEGER,
	usr_role_id_fk INTEGER,
	PRIMARY KEY(usr_user_id_fk,usr_role_id_fk)
);

CREATE TABLE RES_RESOURCE_ORIGIN (
	id INTEGER PRIMARY KEY,
	name VARCHAR(15) NOT NULL
);

CREATE TABLE RES_RESOURCE_IMAGE (
	id SERIAL PRIMARY KEY,
	filename VARCHAR(15) NOT NULL,
	location VARCHAR(255) NOT NULL,
	res_resource_origin_fk INTEGER NOT NULL
);

CREATE TABLE CUS_CUSTOMER (
	id SERIAL PRIMARY KEY,
	name VARCHAR(30) NOT NULL,
	surname VARCHAR(30),
	profile_res_resource_image_fk INTEGER,
	created_by_usr_user_fk INTEGER NOT NULL,
	last_by_usr_user_fk INTEGER NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE
);