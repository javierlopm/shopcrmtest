INSERT INTO USR_ROLE(id,name) VALUES(1,'ADMIN');

INSERT INTO RES_RESOURCE_ORIGIN(id,name) VALUES(1,'FILESYSTEM');
-- userpassword: 1234
-- JUST TO INSERT INTO DEBUG ENV
INSERT INTO USR_USER(username, email, password) VALUES ('admin', 'admin@email.com', '$2a$10$adHA.hiTBUVioLgoQmMMjeWLunDnmxgRzfyX3ZUhMS4SHtkcISCxy');
INSERT INTO USR_USER_ROLE(usr_user_id_fk,usr_role_id_fk) VALUE(1,1);