
all: build

export USER_ID :=  $(shell echo CURRENT_UID=$$(id -u):$$(id -g))

debug:
	$(USER_ID) docker-compose -f docker/debug-docker-compose.yml up

clean:
	docker-compose -f docker/debug-docker-compose.yml down --rmi all --remove-orphans
	docker-compose -f docker/deploy-docker-compose.yml down --rmi all --remove-orphans
	sudo rm -rf db/.var .m2 app/target .srv

build:
	docker-compose -f docker/debug-docker-compose.yml up --force-recreate db &
	$(USER_ID) docker-compose -f docker/deploy-docker-compose.yml up
