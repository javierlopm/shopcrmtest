FROM maven:3.6.3-jdk-11

WORKDIR /usr/src/app

COPY pom.xml .
COPY src ./src
COPY mvnw .

ENTRYPOINT ["mvn","compile","test","package"]