package com.monkey.shopcrm;

import com.monkey.shopcrm.dto.usr.CreateUserDto;
import com.monkey.shopcrm.providers.usr.UserProvider;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional
@SpringBootTest
class ShopcustomersApplicationTests {

	@Autowired
	UserProvider userProvider;

	@Test
	public void testSample() {
		assertTrue(true);
	}

	@Test
	public void userInsertionTest() {
		String username = "aaaaaaaaaaaaaaaaaaaaaaa";
		String password = "Abc123!@#";
		String email = "email@email.com";

		CreateUserDto createUserDto = new CreateUserDto();
		createUserDto.setUsername(username);
		createUserDto.setPassword(password);
		createUserDto.setEmail("email@email.com");

		userProvider.createUser(createUserDto);
	}
}
