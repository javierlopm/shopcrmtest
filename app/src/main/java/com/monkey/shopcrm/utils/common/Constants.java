package com.monkey.shopcrm.utils.common;

public class Constants {
    public static final class ErrorCodes {
        public static final String OBJECT_NOT_FOUND = "OBJECT_NOT_FOUND";
    }
}
