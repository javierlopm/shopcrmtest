package com.monkey.shopcrm.db.usr;

import com.monkey.shopcrm.db.entities.usr.RoleEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoleRepository extends CrudRepository<RoleEntity,Long> {
    List<RoleEntity> findAll();
}
