package com.monkey.shopcrm.db.usr;

import com.monkey.shopcrm.db.entities.usr.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<UserEntity,Long> {
    Page<UserEntity> findByActiveTrue(Pageable p);
    Optional<UserEntity> findByUsernameAndActiveTrue(String username);
    Optional<UserEntity> findByIdAndActiveTrue(Long id);
    Optional<UserEntity> findByEmailAndActiveTrue(String email);
}
