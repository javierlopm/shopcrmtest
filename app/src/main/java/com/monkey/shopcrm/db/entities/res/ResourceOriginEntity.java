package com.monkey.shopcrm.db.entities.res;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "RES_RESOURCE_ORIGIN")
public class ResourceOriginEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Getter @Setter
    @Column
    private String name;
}
