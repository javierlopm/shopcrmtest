package com.monkey.shopcrm.db.entities.cus;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "CUS_CUSTOMER")
public class CustomerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Column
    @Getter @Setter
    private String name;

    @Column
    @Getter @Setter
    private String surname;

    @Column(name = "created_by_usr_user_fk")
    @Getter @Setter
    private Long createdBy;

    @Column(name = "last_by_usr_user_fk")
    @Getter @Setter
    private Long lastModifiedBy;

    @Column(name = "profile_res_resource_image_fk")
    @Getter @Setter
    private Long profileResourceImage;

    @Column
    @Getter @Setter
    private Boolean active = true;
}
