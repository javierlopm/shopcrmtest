package com.monkey.shopcrm.db.entities.usr;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity(name = "USR_USER")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Column(unique = true)
    @Getter @Setter
    private String username;

    @Column(unique = true)
    @Getter @Setter
    private String email;

    @Column
    @Getter @Setter
    private String password;

    @Column
    @Getter @Setter
    private Boolean active = true;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "USR_USER_ROLE",
        joinColumns = @JoinColumn(name="USR_USER_ID_FK") ,
        inverseJoinColumns = @JoinColumn(name="USR_ROLE_ID_FK"))
    @Getter @Setter
    private List<RoleEntity> roles;
}
