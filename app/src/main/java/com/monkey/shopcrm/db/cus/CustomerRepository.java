package com.monkey.shopcrm.db.cus;

import com.monkey.shopcrm.db.entities.cus.CustomerEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CustomerRepository extends CrudRepository<CustomerEntity,Long> {
    Page<CustomerEntity> findByActiveTrue(Pageable p);
    Optional<CustomerEntity> findByIdAndActiveTrue(Long id);
}
