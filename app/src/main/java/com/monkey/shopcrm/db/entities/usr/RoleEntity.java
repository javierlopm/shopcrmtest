package com.monkey.shopcrm.db.entities.usr;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "USR_ROLE")
public class RoleEntity {
    @Id
    @Getter
    Long id;

    @Getter @Setter
    String name;

    public RoleEntity(Long id){
        this.id = id;
    }
}
