package com.monkey.shopcrm.db.res;

import com.monkey.shopcrm.db.entities.res.ResourceImageEntity;
import org.springframework.data.repository.CrudRepository;

public interface ResourceImageRepository extends CrudRepository<ResourceImageEntity,Long> {
}
