package com.monkey.shopcrm.db.entities.res;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "RES_RESOURCE_IMAGE")
public class ResourceImageEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Getter @Setter
    @Column
    private String filename;

    @Getter @Setter
    @Column
    private String location;

//    @ManyToOne
//    @JoinColumn(name="res_resource_origin_fk",nullable = false)
    //    private ResourceOriginEntity resourceOrigin;
    @Column(name="res_resource_origin_fk",nullable = false)
    @Getter @Setter
    private Long resourceOrigin;


}
