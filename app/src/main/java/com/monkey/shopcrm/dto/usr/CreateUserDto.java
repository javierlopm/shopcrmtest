package com.monkey.shopcrm.dto.usr;

import com.monkey.shopcrm.validators.usr.StrongPassword;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class CreateUserDto {
    @Getter @Setter
    @NotEmpty
    @Size(min = 5,max = 50)
    private String username;

    @NotEmpty
    @Email
    @Getter @Setter
    private String email;

    @NotEmpty
    @Getter @Setter
    @StrongPassword
    private String password;
}
