package com.monkey.shopcrm.dto.auth;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class UserPrincipalDto {
    @Getter @Setter
    private String username;
    @Getter @Setter
    private Long id;
    @Getter @Setter
    private List<String> roles;

    public String toString(){
        return String.valueOf(id);
    }
}
