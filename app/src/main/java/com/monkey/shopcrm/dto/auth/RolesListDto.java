package com.monkey.shopcrm.dto.auth;

import lombok.*;

import java.util.List;

@Data
public class RolesListDto {
    private List<Long> roles;
}
