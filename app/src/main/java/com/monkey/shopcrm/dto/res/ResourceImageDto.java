package com.monkey.shopcrm.dto.res;

import lombok.Getter;
import lombok.Setter;

public class ResourceImageDto {
    @Getter @Setter
    private Long id;

    @Getter @Setter
    private String filename;

    @Getter @Setter
    private byte[] file;
}
