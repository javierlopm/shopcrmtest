package com.monkey.shopcrm.dto.cus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class CustomerProfilePictureDto {
    @Getter @Setter
    private String filename;

    @Getter @Setter
    private byte[] file;
}
