package com.monkey.shopcrm.dto.cus;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {
    @Getter
    @Setter
    private Long id;

    @NotEmpty
    @Size(min=2,max=30)
    @Getter @Setter
    private String name;

    @Size(min=2,max=30)
    @Getter @Setter
    private String surname;

    private Long profileResourceImage;

    @JsonIgnore
    public void setProfileResourceImage(Long profileResourceImage) {
        this.profileResourceImage = profileResourceImage;
    }

    @JsonIgnore
    public Long getProfileResourceImage() {
        return this.profileResourceImage;
    }
}
