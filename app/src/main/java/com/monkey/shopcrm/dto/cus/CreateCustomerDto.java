package com.monkey.shopcrm.dto.cus;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class CreateCustomerDto {
    @NotEmpty
    @Size(min=2,max=30)
    @Getter @Setter
    private String name;

    @Column
    @Size(min=2,max=30)
    @Getter @Setter
    private String surname;
}
