package com.monkey.shopcrm.dto.common;

public class ApplicationException extends RuntimeException {

    public ApplicationException(String message){
        super(message);
    }

}
