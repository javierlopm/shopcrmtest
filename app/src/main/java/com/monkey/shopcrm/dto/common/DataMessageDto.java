package com.monkey.shopcrm.dto.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class DataMessageDto<T> {
    @Getter @Setter
    T data;

    @Getter @Setter
    boolean error = false;

    @Getter @Setter
    List<String> errors = null;

    public DataMessageDto(T data){
        this.data = data;
    }
}
