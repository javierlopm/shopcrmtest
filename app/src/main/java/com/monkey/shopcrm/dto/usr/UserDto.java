package com.monkey.shopcrm.dto.usr;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    @Getter
    @Setter
    private Long id;

    @Getter @Setter
    private String username;

    @Getter @Setter
    private String email;

}
