package com.monkey.shopcrm.dto.auth;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@NoArgsConstructor
public class UserCredentialsDto {
    @Setter
    @NotEmpty
    String username;

    @Setter
    @NotEmpty
    String password;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
