package com.monkey.shopcrm.dto.auth;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

@AllArgsConstructor
public class GrantedAuthorityDto implements GrantedAuthority {
    @Getter
    public String authority;
}
