package com.monkey.shopcrm.controllers.cus;

import com.monkey.shopcrm.dto.common.ApplicationException;
import com.monkey.shopcrm.dto.cus.CreateCustomerDto;
import com.monkey.shopcrm.dto.cus.CustomerDto;
import com.monkey.shopcrm.dto.cus.CustomerProfilePictureDto;
import com.monkey.shopcrm.providers.cus.CustomerProvider;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.security.Principal;

@Slf4j
@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    CustomerProvider customerProvider;

    @GetMapping("/")
    @PageableAsQueryParam // Openapi doc bug workaround
    public Page<CustomerDto> getAllCustomers(@PageableDefault @Parameter(hidden=true) Pageable pageable) {
        return customerProvider.getAllCustomers(pageable);
    }

    @GetMapping("/{id}")
    public CustomerDto getCustomerById(@PathVariable long id) {
        return customerProvider.getCustomerById(id);
    }

    @PostMapping("/")
    public Long createCustomer(@RequestBody @Valid CreateCustomerDto customerDto, Principal principal, Errors errors){
        return customerProvider.createCustomer(customerDto,Long.valueOf(principal.getName()));
    }

    @PutMapping("/{id}")
    public Long editCustomer(@PathVariable long id, @RequestBody @Valid CreateCustomerDto customerDto, Principal principal){
        return customerProvider.updateCustomer(id,customerDto,Long.valueOf(principal.getName()));
    }

    @DeleteMapping("/{id}")
    public Long deleteCustomer(@PathVariable long id, Principal principal){
        return customerProvider.deleteCustomer(id,Long.valueOf(principal.getName()));
    }

    @PutMapping(value = "/{id}/profile-picture",consumes = {"multipart/form-data"})
    public Long putProfilePicture(@PathVariable long id, @RequestBody @Valid @NotNull MultipartFile file, Principal principal){
        byte [] fileContent;

        try {
            fileContent = file.getBytes();
        } catch (IOException e) {
            log.error("Could not read bytes from request.",e);
            throw new ApplicationException("Could not read bytes from request.");
        }

        CustomerProfilePictureDto customerProfilePictureDto = new CustomerProfilePictureDto();
        customerProfilePictureDto.setFile(fileContent);
        customerProfilePictureDto.setFilename(file.getOriginalFilename());

        return customerProvider.putProfilePicture(id,customerProfilePictureDto,Long.valueOf(principal.getName()));
    }

    @GetMapping("/{id}/profile-picture")
    public byte[] getProfilePicture(@PathVariable long id, HttpServletResponse response) {
        CustomerProfilePictureDto profilePicture = customerProvider.getProfilePicture(id);
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=" + profilePicture.getFilename());
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        return profilePicture.getFile();
    }
}
