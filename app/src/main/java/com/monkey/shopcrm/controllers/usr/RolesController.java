package com.monkey.shopcrm.controllers.usr;

import com.monkey.shopcrm.dto.usr.RoleDto;
import com.monkey.shopcrm.providers.usr.RoleProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/roles")
public class RolesController {
    @Autowired
    RoleProvider roleProvider;

    @GetMapping("/")
    public List<RoleDto> getRoles(){
        return roleProvider.getAllRoles();
    }
}
