package com.monkey.shopcrm.controllers.common;

import com.monkey.shopcrm.dto.common.ApplicationException;
import com.monkey.shopcrm.dto.common.DataMessageDto;
import com.monkey.shopcrm.dto.common.ErrorDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.AccessDeniedException;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ExceptionHandlerController {

    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    @ExceptionHandler(ApplicationException.class)
    public DataMessageDto<String> handleAppError(HttpServletRequest req, Exception ex) {
        DataMessageDto<String> messageDto = new DataMessageDto<>();
        messageDto.setErrors(List.of(ex.getMessage()));
        messageDto.setError(true);
        log.error("Application error",ex);
        return messageDto;
    }

    // Field validation objects
    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorDto methodArgumentNotValidException(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        return processFieldErrors(fieldErrors);
    }

    private ErrorDto processFieldErrors(List<org.springframework.validation.FieldError> fieldErrors) {
        ErrorDto error = new ErrorDto(BAD_REQUEST.value(), "validation error");
        for (FieldError fieldError: fieldErrors) {
            error.addFieldError(fieldError.getField(), fieldError.getDefaultMessage());
        }
        return error;
    }

    @ResponseStatus(FORBIDDEN)
    @ResponseBody
    @ExceptionHandler(AccessDeniedException.class)
    public DataMessageDto<String> handleError(HttpServletRequest req, AccessDeniedException ex) {
        DataMessageDto<String> messageDto = new DataMessageDto<>();
        messageDto.setErrors(List.of(ex.getMessage()));
        messageDto.setError(true);

        log.error("Forbidden access was tried. With address: " + req.getRemoteAddr() ,ex);

        return messageDto;
    }

    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public DataMessageDto<String> handleError(HttpServletRequest req, Exception ex) {
        log.error("Request: " + req.getRequestURL() + " raised ",ex);

        DataMessageDto<String> messageDto = new DataMessageDto<>();
        messageDto.setErrors(List.of(ex.getMessage()));
        messageDto.setError(true);

        return messageDto;
    }
}
