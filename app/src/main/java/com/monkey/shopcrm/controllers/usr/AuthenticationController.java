package com.monkey.shopcrm.controllers.usr;

import com.monkey.shopcrm.dto.auth.UserCredentialsDto;
import com.monkey.shopcrm.providers.auth.AuthenticationProvider;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Log4j2
@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    AuthenticationProvider authenticationProvider;

    @PostMapping("/login")
    @Operation(summary = "userAuthLogin", description = "For test username:'admin' and password:'1234'")
    public String userAuthLogin(@RequestBody UserCredentialsDto userCredentialsDto){
        return authenticationProvider.getJwtFromUserCredentials(userCredentialsDto);
    }

    @GetMapping("/refreshToken")
    @Operation(summary = "Refresh token", description = "This end-point gives you a fresh token with new expiration date. You must be authenticated")
    public String userAuthLogin(Principal principal){
        return authenticationProvider.getJwtFromUserId(Long.valueOf(principal.getName()));
    }
}
