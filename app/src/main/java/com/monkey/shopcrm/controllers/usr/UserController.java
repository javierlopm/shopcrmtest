package com.monkey.shopcrm.controllers.usr;

import com.monkey.shopcrm.dto.auth.RolesListDto;
import com.monkey.shopcrm.dto.usr.CreateUserDto;
import com.monkey.shopcrm.dto.usr.RoleDto;
import com.monkey.shopcrm.dto.usr.UserDto;
import com.monkey.shopcrm.providers.usr.UserProvider;
import io.swagger.v3.oas.annotations.Parameter;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserProvider userProvider;

    @GetMapping("/")
    @PreAuthorize("hasAuthority('ADMIN')")
    @PageableAsQueryParam // Openapi doc bug workaround
    public Page<UserDto> getAllUser(@Parameter(hidden=true)  @PageableDefault Pageable pageable) {
        return userProvider.getAllUsers(pageable);
    }

    @GetMapping("/{id}")
    public UserDto getUserById(@PathVariable long id) {
        return userProvider.getUserById(id);
    }

    @PostMapping("/")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Long createUser(@Valid @RequestBody CreateUserDto createUserDto){
        return userProvider.createUser(createUserDto);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Long editUser(@PathVariable long id, @RequestBody CreateUserDto createUserDto){
        return userProvider.updateUser(id,createUserDto);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Long deleteUser(@PathVariable long id){
        return userProvider.deleteUser(id);
    }

    @PutMapping("/{id}/roles")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Long setUserRoles(@PathVariable long id, @RequestBody RolesListDto rolesListDto) {
        return userProvider.setRoles(id,rolesListDto);
    }

    @GetMapping("/{id}/roles")
    @PreAuthorize("hasAuthority('ADMIN')")
    public List<RoleDto> getUserRoles(@PathVariable long id) {
        return userProvider.getRoles(id);
    }
}
