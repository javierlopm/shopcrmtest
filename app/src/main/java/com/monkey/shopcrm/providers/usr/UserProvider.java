package com.monkey.shopcrm.providers.usr;

import com.monkey.shopcrm.dto.auth.RolesListDto;
import com.monkey.shopcrm.dto.usr.CreateUserDto;
import com.monkey.shopcrm.dto.usr.RoleDto;
import com.monkey.shopcrm.dto.usr.UserDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserProvider {

    Page<UserDto> getAllUsers(Pageable p);

    UserDto getUserById(long userId);

    long createUser(CreateUserDto createUserDto);

    long updateUser(long userId, CreateUserDto createUserDto);

    long deleteUser(long userId);

    long setRoles(Long userId, RolesListDto rolesListDto);

    List<RoleDto> getRoles(Long userId);

}
