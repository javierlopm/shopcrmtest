package com.monkey.shopcrm.providers.cus;

import com.monkey.shopcrm.dto.cus.CreateCustomerDto;
import com.monkey.shopcrm.dto.cus.CustomerDto;
import com.monkey.shopcrm.dto.cus.CustomerProfilePictureDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CustomerProvider {
    Page<CustomerDto> getAllCustomers(Pageable pageable);
    CustomerDto getCustomerById(long id);
    Long createCustomer(CreateCustomerDto customerDto, Long userId);
    Long updateCustomer(long id, CreateCustomerDto customerDto, Long userId);
    Long deleteCustomer(long id, Long userId);

    CustomerProfilePictureDto getProfilePicture(long id);

    Long putProfilePicture(long customerId, CustomerProfilePictureDto profilePictureDto, Long userId);
}
