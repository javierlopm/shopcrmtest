package com.monkey.shopcrm.providers.impl.auth;

import com.monkey.shopcrm.db.entities.usr.RoleEntity;
import com.monkey.shopcrm.db.entities.usr.UserEntity;
import com.monkey.shopcrm.db.usr.UserRepository;
import com.monkey.shopcrm.dto.auth.UserCredentialsDto;
import com.monkey.shopcrm.dto.auth.UserPrincipalDto;
import com.monkey.shopcrm.providers.JwtProvider;
import com.monkey.shopcrm.providers.auth.AuthenticationProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
public class AuthenticationProviderImpl implements AuthenticationProvider {
    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtProvider jwtProvider;

    private static final String WRONG_AUTH = "WRONG_AUTH";

    @Override
    public String getJwtFromUserCredentials(UserCredentialsDto userCredentialsDto){
        Optional<UserEntity> userFound = userRepository.findByUsernameAndActiveTrue(userCredentialsDto.getUsername());
        UserEntity user = userFound.orElseThrow(() -> new AccessDeniedException(WRONG_AUTH));
        String encodedPassword = user.getPassword();

        if (!passwordEncoder.matches(userCredentialsDto.getPassword(), encodedPassword)) {
            throw new AccessDeniedException(WRONG_AUTH);
        }

        UserPrincipalDto userPrincipalDto = new UserPrincipalDto();
        userPrincipalDto.setId(user.getId());
        userPrincipalDto.setRoles(user.getRoles().stream().map(RoleEntity::getName).collect(Collectors.toList()));
        userPrincipalDto.setUsername(user.getUsername());

        return jwtProvider.generateToken(userPrincipalDto);
    }

    @Override
    public String getJwtFromUserId(Long userId){
        UserEntity user = userRepository.findByIdAndActiveTrue(userId).orElseThrow(() -> new AccessDeniedException(WRONG_AUTH));

        UserPrincipalDto userPrincipalDto = new UserPrincipalDto();
        userPrincipalDto.setId(user.getId());
        userPrincipalDto.setRoles(new ArrayList<>()); // Cambiar por lo que venga del entity
        userPrincipalDto.setUsername(user.getUsername());

        return jwtProvider.generateToken(userPrincipalDto);
    }
}
