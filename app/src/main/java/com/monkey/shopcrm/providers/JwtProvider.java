package com.monkey.shopcrm.providers;

import com.monkey.shopcrm.dto.auth.UserPrincipalDto;

public interface JwtProvider {
    UserPrincipalDto getPrincipalFromToken(String token);
    String generateToken(UserPrincipalDto userPrincipalDto);
}
