package com.monkey.shopcrm.providers.usr.impl;

import com.monkey.shopcrm.db.entities.usr.RoleEntity;
import com.monkey.shopcrm.db.usr.RoleRepository;
import com.monkey.shopcrm.dto.usr.RoleDto;
import com.monkey.shopcrm.providers.usr.RoleProvider;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RoleProviderImpl implements RoleProvider {
    @Autowired
    RoleRepository roleRepository;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public List<RoleDto> getAllRoles() {
        List<RoleEntity> allEntities = roleRepository.findAll();
        return modelMapper.map(allEntities,new TypeToken<List<RoleDto>>() {}.getType());
    }
}
