package com.monkey.shopcrm.providers.usr.impl;

import com.monkey.shopcrm.db.entities.usr.RoleEntity;
import com.monkey.shopcrm.db.entities.usr.UserEntity;
import com.monkey.shopcrm.db.usr.UserRepository;
import com.monkey.shopcrm.dto.auth.RolesListDto;
import com.monkey.shopcrm.dto.common.ApplicationException;
import com.monkey.shopcrm.dto.usr.CreateUserDto;
import com.monkey.shopcrm.dto.usr.RoleDto;
import com.monkey.shopcrm.dto.usr.UserDto;
import com.monkey.shopcrm.providers.usr.UserProvider;
import com.monkey.shopcrm.utils.common.Constants;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
public class UserProviderImpl implements UserProvider {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    UserRepository userRepository;

    @Override
    public Page<UserDto> getAllUsers(Pageable p) {
        Page<UserEntity> allEntities = userRepository.findByActiveTrue(p);
        List<UserDto> allDtos =
            modelMapper.map(allEntities.getContent(),new TypeToken<List<UserDto>>() {}.getType());
        return new PageImpl<>(allDtos,p,allEntities.getTotalElements());
    }

    @Override
    public UserDto getUserById(long userId) {
        Optional<UserEntity> userFound = userRepository.findByIdAndActiveTrue(userId);
        UserEntity user = userFound.orElseThrow(() -> new ApplicationException(Constants.ErrorCodes.OBJECT_NOT_FOUND));
        return modelMapper.map(user,UserDto.class);
    }

    @Override
    public long createUser(CreateUserDto createUserDto) {
        // Sorry guys, quick solution.
        // This MUST be move to a validator, it could be checked on a controller level expression
        // Validation is NOT a concern of this class
        if (userRepository.findByEmailAndActiveTrue(createUserDto.getEmail()).isPresent()) {
            throw new ApplicationException("EMAIL_EXISTS");
        }

        if (userRepository.findByUsernameAndActiveTrue(createUserDto.getUsername()).isPresent()) {
            throw new ApplicationException("USERNAME_EXISTS");
        }

        UserEntity userEntity = modelMapper.map(createUserDto, UserEntity.class);
        userEntity.setPassword(passwordEncoder.encode(createUserDto.getPassword()));
        userEntity = userRepository.save(userEntity);

        return userEntity.getId();
    }

    @Override
    public long updateUser(long userId, CreateUserDto createUserDto) {
        UserEntity userEntity =
            userRepository
                .findByIdAndActiveTrue(userId)
                .orElseThrow(() -> new ApplicationException(Constants.ErrorCodes.OBJECT_NOT_FOUND));

        // This MUST be move to a validator, it could be checked on PreAuthorize
        Optional<UserEntity> userByEmail = userRepository.findByEmailAndActiveTrue(createUserDto.getEmail());
        if (userByEmail.isPresent() && userByEmail.get().getId() != userId) {
            throw new ApplicationException("EMAIL_EXISTS");
        }

        Optional<UserEntity> userByUsername = userRepository.findByUsernameAndActiveTrue(createUserDto.getUsername());
        if (userByUsername.isPresent() && userByUsername.get().getId() != userId) {
            throw new ApplicationException("USERNAME_EXISTS");
        }

        userEntity.setUsername(createUserDto.getUsername());
        userEntity.setEmail(createUserDto.getEmail());
        userEntity = userRepository.save(userEntity);

        return userEntity.getId();
    }

    @Override
    public long deleteUser(long userId) {
        Optional<UserEntity> userFound = userRepository.findByIdAndActiveTrue(userId);
        UserEntity userEntity = userFound.orElseThrow(() -> new ApplicationException(Constants.ErrorCodes.OBJECT_NOT_FOUND));
        userEntity.setActive(false);
        userRepository.save(userEntity);
        return userId;
    }

    @Override
    public long setRoles(Long userId, RolesListDto rolesListDto) {
        Optional<UserEntity> userFound = userRepository.findByIdAndActiveTrue(userId);
        UserEntity userEntity = userFound.orElseThrow(() -> new ApplicationException(Constants.ErrorCodes.OBJECT_NOT_FOUND));
        userEntity.getRoles().clear();
        List<RoleEntity> newRoles = rolesListDto.getRoles().stream().map(RoleEntity::new).collect(Collectors.toList());
        userEntity.getRoles().addAll(newRoles);
        userRepository.save(userEntity);
        return userId;
    }

    @Override
    public List<RoleDto> getRoles(Long userId) {
        Optional<UserEntity> userFound = userRepository.findByIdAndActiveTrue(userId);
        UserEntity userEntity = userFound.orElseThrow(() -> new ApplicationException(Constants.ErrorCodes.OBJECT_NOT_FOUND));
        return modelMapper.map(userEntity.getRoles(),new TypeToken<List<RoleDto>>() {}.getType());
    }

}
