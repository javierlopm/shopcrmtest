package com.monkey.shopcrm.providers.res.impl;

import com.monkey.shopcrm.db.entities.res.ResourceImageEntity;
import com.monkey.shopcrm.db.res.ResourceImageRepository;
import com.monkey.shopcrm.dto.common.ApplicationException;
import com.monkey.shopcrm.dto.res.ResourceImageDto;
import com.monkey.shopcrm.providers.res.ResourceImageProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Optional;
import java.util.Random;

@Slf4j
@Component
public class FilesystemResourceImageProviderImpl implements ResourceImageProvider {

    public static final long FILESYSTEM_RESOURCE_ORIGIN = 1L;

    @Value("${local-resources.images-directory}")
    private String imagesDirectory;

    @Autowired
    ResourceImageRepository resourceImageRepository;

    @Override
    public ResourceImageDto getImage(Long profileImageId) {
        Optional<ResourceImageEntity> resourceImageEntity = resourceImageRepository.findById(profileImageId);
        if (resourceImageEntity.isEmpty()) {
            throw new ApplicationException("No image to download");
        }

        ResourceImageDto resourceImageDto = new ResourceImageDto();
        resourceImageDto.setId(profileImageId);
        byte[] fileContent;
        try {
            fileContent = Files.readAllBytes(Paths.get(resourceImageEntity.get().getLocation()));
        } catch (IOException e) {
            log.error("Could not read file",e);
            throw new ApplicationException("Cold not read file");
        }
        resourceImageDto.setFile(fileContent);
        resourceImageDto.setFilename(resourceImageEntity.get().getFilename());

        return resourceImageDto;
    }

    @Override
    public Long createResourceImage(ResourceImageDto resourceImage) {
        Path filepath = Paths.get(imagesDirectory, generateUniqueFilename());

        try (FileOutputStream stream = new FileOutputStream(filepath.toFile())) {
            stream.write(resourceImage.getFile());
        } catch (IOException e) {
            log.error("Could not store resource image!",e);
            throw new ApplicationException("Could not store resource image!");
        }

        ResourceImageEntity resourceImageEntity = new ResourceImageEntity();
        resourceImageEntity.setFilename(resourceImage.getFilename());
        resourceImageEntity.setResourceOrigin(FILESYSTEM_RESOURCE_ORIGIN);
        resourceImageEntity.setLocation(filepath.toString());

        resourceImageEntity = resourceImageRepository.save(resourceImageEntity);

        return resourceImageEntity.getId();
    }

    @Override
    public Long updateResourceImage(ResourceImageDto resourceImage) {
        return null;
    }

    private String generateUniqueFilename() {
        String randomNumber = String.valueOf(Math.abs(new Random().nextInt()));
        String timestamp = String.valueOf(new Date().getTime());

        return String.format("%s_%s",randomNumber,timestamp);
    }

}
