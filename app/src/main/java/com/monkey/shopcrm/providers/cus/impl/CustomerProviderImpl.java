package com.monkey.shopcrm.providers.cus.impl;

import com.monkey.shopcrm.db.cus.CustomerRepository;
import com.monkey.shopcrm.db.entities.cus.CustomerEntity;
import com.monkey.shopcrm.dto.common.ApplicationException;
import com.monkey.shopcrm.dto.cus.CreateCustomerDto;
import com.monkey.shopcrm.dto.cus.CustomerDto;
import com.monkey.shopcrm.dto.cus.CustomerProfilePictureDto;
import com.monkey.shopcrm.dto.res.ResourceImageDto;
import com.monkey.shopcrm.providers.cus.CustomerProvider;
import com.monkey.shopcrm.providers.res.ResourceImageProvider;
import com.monkey.shopcrm.utils.common.Constants;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class CustomerProviderImpl implements CustomerProvider {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ResourceImageProvider resourceImageProvider;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public Page<CustomerDto> getAllCustomers(Pageable pageable) {
        Page<CustomerEntity> allEntities = customerRepository.findByActiveTrue(pageable);
        List<CustomerDto> allDtos =
            modelMapper.map(allEntities.getContent(),new TypeToken<List<CustomerDto>>() {}.getType());
        return new PageImpl<>(allDtos,pageable,allEntities.getTotalElements());
    }

    @Override
    public CustomerDto getCustomerById(long id) {
        Optional<CustomerEntity> customerFound = customerRepository.findByIdAndActiveTrue(id);
        CustomerEntity customer = customerFound.orElseThrow(() -> new ApplicationException(Constants.ErrorCodes.OBJECT_NOT_FOUND));
        return modelMapper.map(customer,CustomerDto.class);
    }

    @Override
    public Long createCustomer(CreateCustomerDto customerDto, Long userId) {
        CustomerEntity customerEntity = modelMapper.map(customerDto, CustomerEntity.class);
        customerEntity.setLastModifiedBy(userId);
        customerEntity.setCreatedBy(userId);
        customerEntity = customerRepository.save(customerEntity);
        return customerEntity.getId();
    }

    @Override
    public Long updateCustomer(long id, CreateCustomerDto customerDto, Long userId) {
        CustomerEntity customerEntity =
                customerRepository
                        .findByIdAndActiveTrue(id)
                        .orElseThrow(() -> new ApplicationException(Constants.ErrorCodes.OBJECT_NOT_FOUND));

        modelMapper.map(customerDto,customerEntity);
        customerEntity.setLastModifiedBy(userId);
        customerEntity = customerRepository.save(customerEntity);

        return customerEntity.getId();
    }

    @Override
    public Long deleteCustomer(long id, Long userId) {
        Optional<CustomerEntity> customerFound = customerRepository.findByIdAndActiveTrue(id);
        CustomerEntity customerEntity = customerFound.orElseThrow(() -> new ApplicationException(Constants.ErrorCodes.OBJECT_NOT_FOUND));
        customerEntity.setActive(false);
        customerEntity.setLastModifiedBy(userId);
        customerRepository.save(customerEntity);
        return id;
    }

    @Override
    public CustomerProfilePictureDto getProfilePicture(long id){
        Optional<CustomerEntity> customerFound = customerRepository.findByIdAndActiveTrue(id);
        Long profileImageId = customerFound.map(CustomerEntity::getProfileResourceImage)
                .orElseThrow(() -> new ApplicationException(Constants.ErrorCodes.OBJECT_NOT_FOUND));

        ResourceImageDto resourceImageDto = resourceImageProvider.getImage(profileImageId);

        return modelMapper.map(resourceImageDto,CustomerProfilePictureDto.class);
    }

    @Override
    public Long putProfilePicture(long customerId,CustomerProfilePictureDto profilePictureDto, Long userId){
        Optional<CustomerEntity> customerFound = customerRepository.findByIdAndActiveTrue(customerId);
        CustomerEntity customerEntity = customerFound.orElseThrow(() -> new ApplicationException(Constants.ErrorCodes.OBJECT_NOT_FOUND));

        Long profileImageId = customerEntity.getProfileResourceImage();

        ResourceImageDto resourceImage = modelMapper.map(profilePictureDto, ResourceImageDto.class);

        if (profileImageId == null) {
            profileImageId = resourceImageProvider.createResourceImage(resourceImage);
        }
        else {
            resourceImage.setId(profileImageId);
            profileImageId = resourceImageProvider.updateResourceImage(resourceImage);
        }

        customerEntity.setProfileResourceImage(profileImageId);
        customerEntity.setLastModifiedBy(userId);
        customerRepository.save(customerEntity);

        return customerId;
    }
}
