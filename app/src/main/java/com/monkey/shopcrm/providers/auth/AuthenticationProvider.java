package com.monkey.shopcrm.providers.auth;

import com.monkey.shopcrm.dto.auth.UserCredentialsDto;

public interface AuthenticationProvider {
    String getJwtFromUserCredentials(UserCredentialsDto userCredentialsDto);

    String getJwtFromUserId(Long userId);
}
