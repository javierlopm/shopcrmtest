package com.monkey.shopcrm.providers.res;

import com.monkey.shopcrm.dto.res.ResourceImageDto;

public interface ResourceImageProvider {
    ResourceImageDto getImage(Long profileImageId);
    Long createResourceImage(ResourceImageDto resourceImage);
    Long updateResourceImage(ResourceImageDto resourceImage);
}
