package com.monkey.shopcrm.providers.impl.auth;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.monkey.shopcrm.dto.auth.UserPrincipalDto;
import com.monkey.shopcrm.providers.JwtProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;

@Component
public class JwtProviderImpl implements JwtProvider {

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.issuer}")
    private String issuer;

    @Value("${jwt.minutes-validity}")
    private long jwtMinutesValidity;

    private static final String BEARER = "Bearer ";
    private static final String USER_ID_CLAIM = "userId";

    public static final long MINUTE = 60 * 60 * 1000;

    private Date tokenValidityDate(){
        return new Date( (new Date()).getTime() + MINUTE *  jwtMinutesValidity);

    }

    private Algorithm getAlgorithm(){
        return Algorithm.HMAC512(secret);
    }

    private JWTVerifier getVerifier(){
        return JWT.require(getAlgorithm())
                .withIssuer(issuer)
                .build();
    }

    @Override
    public UserPrincipalDto getPrincipalFromToken(String token) {
        DecodedJWT jwt = getVerifier().verify(token);

        return new UserPrincipalDto(
                jwt.getSubject(),
                jwt.getClaim(USER_ID_CLAIM).asLong(),
                new ArrayList<>());
    }

    @Override
    public String generateToken(UserPrincipalDto userPrincipalDto) {
        String jwtToken = JWT.create()
            .withSubject(userPrincipalDto.getUsername())
            .withIssuer(issuer)
            .withClaim(USER_ID_CLAIM,userPrincipalDto.getId())
            .withExpiresAt(tokenValidityDate())
            .sign(getAlgorithm());
        return BEARER + jwtToken;
    }
}
