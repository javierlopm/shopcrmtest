package com.monkey.shopcrm.providers.usr;

import com.monkey.shopcrm.dto.usr.RoleDto;

import java.util.List;

public interface RoleProvider {
    List<RoleDto> getAllRoles();
}
