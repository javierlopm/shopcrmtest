package com.monkey.shopcrm.config.security;

import com.monkey.shopcrm.dto.auth.GrantedAuthorityDto;
import com.monkey.shopcrm.dto.auth.UserPrincipalDto;
import com.monkey.shopcrm.providers.JwtProvider;
import com.monkey.shopcrm.providers.usr.UserProvider;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class JwtAuthFilter extends OncePerRequestFilter {

    private JwtProvider jwtProvider;
    private UserProvider userProvider;

    private static final String BEARER = "Bearer ";
    private static final String AUTHORIZATION_HEADER = "Authorization";

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws IOException, ServletException {
        String authorizationHeader = httpServletRequest.getHeader(AUTHORIZATION_HEADER);

        if (authorizationHeaderIsInvalid(authorizationHeader)) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }

        UsernamePasswordAuthenticationToken token = createToken(authorizationHeader);

        SecurityContextHolder.getContext().setAuthentication(token);
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private boolean authorizationHeaderIsInvalid(String authorizationHeader) {
        return authorizationHeader == null
                || !authorizationHeader.startsWith(BEARER);
    }

    private UsernamePasswordAuthenticationToken createToken(String authorizationHeader) {
        String token = authorizationHeader.replace(BEARER, "");
        UserPrincipalDto userPrincipal = jwtProvider.getPrincipalFromToken(token);

        List<GrantedAuthority> authorities = userProvider.getRoles(userPrincipal.getId()).stream().map(r ->new GrantedAuthorityDto(r.getName())).collect(Collectors.toList());

        return new UsernamePasswordAuthenticationToken(userPrincipal, null, authorities);
    }
}

