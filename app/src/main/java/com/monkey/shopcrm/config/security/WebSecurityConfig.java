package com.monkey.shopcrm.config.security;


import com.monkey.shopcrm.providers.JwtProvider;
import com.monkey.shopcrm.providers.usr.UserProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${SWAGGER_UI_ENABLED}")
    Boolean swaggerUiEnabled;
    @Value("${SWAGGER_UI_PATH}")
    String swaggerUiPath;

    @Value("${API_DOC_ENABLED}")
    Boolean apiDocEnabled;
    @Value("${API_DOC_PATH}")
    String apiDocPath;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    UserProvider userProvider;

    protected void configure(HttpSecurity http) throws Exception {
        http.headers().xssProtection().xssProtectionEnabled(true);

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                .antMatchers("/auth/login","error").permitAll()
                .and().csrf().disable()
                .formLogin().disable()
                .cors().disable()
                .addFilterBefore(new JwtAuthFilter(jwtProvider,userProvider), UsernamePasswordAuthenticationFilter.class);

        if (swaggerUiEnabled) {
            http.authorizeRequests()
                    .antMatchers("/swagger-ui/**").permitAll()
                    .antMatchers(swaggerUiPath).permitAll();
        }

        if (apiDocEnabled) {
            http.authorizeRequests().antMatchers(apiDocPath+"/**").permitAll();
        }

        http.authorizeRequests().anyRequest().authenticated();
    }

}
