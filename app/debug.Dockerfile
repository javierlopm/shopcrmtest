FROM maven:3.6.3-jdk-11

WORKDIR /usr/src/app

ENTRYPOINT ["mvn","spring-boot:run","-Dagentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005"]