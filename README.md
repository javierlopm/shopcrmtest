# Getting Started

Install docker, your prefered IDE for java/spring and that's it, you can see the *Requirements* section
for more info.

To Start the project (in Linux) just write "make debug" in the console and that will create the database
schemas, users, and populate it to start right away,

# Requirements

Install docker and docker-compose: https://docs.docker.com/compose/install/

# How to compile for production

You can simple use the "make" command in the project's root directory or do the following:

First start a local DB for tests with the following command:

docker-compose -f docker/debug-docker-compose.yml up --force-recreate db

The following command will compile, run tests and store the jar file in app/target:

docker-compose -f docker/deploy-docker-compose.yml up


Before deploy remember:

Generate new JWT_SECRET for config.env only for deployment with the following command:
openssl rand -base64 172 | tr -d '\n'

Adjust DB env variables to deploy DB, deactivate swagger.

Double check the user "admin"-"1234" is not inserted in the DB, hehe.


# How to run for local dev

In the project root run the following commands

- First database: docker-compose -f docker/debug-docker-compose.yml up db
- Then the spring app: docker-compose -f docker/debug-docker-compose.yml up shopcrm-debug

That's it. Now open your Java IDE and you make changes in the source code and they will be recompiled.
You can connect for remote debugging on the default port (5005). Alternatively you can run the "make debug" command.

You can clean all container by using "make clean".

# Check endpoints

0. Visit http://localhost:8080/swagger-ui.html
1. Go to the login end-point and use "admin" as user and "1234 as password", copy the response, is the Bearer token.
2. Click the lock on any other documented end-point and paste the token.
3. Start playing with the API :) 

# Requirements to develop with  IntelliJ
 
* Install Lombok plugin
    * Accept the "enable annotation"

# Technologies used

- Postgres + (optional) pgADMIN
- Docker & Dockercompose
- Spring boot
- Git
- IntelliJ

## Packages

- Openapi doc: To use have swagger doc. It has a small problem with pageables (https://github.com/springdoc/springdoc-openapi/issues/177)
- ModelMapper: to copy properties easily between data transfer objects and entities.
- Lombok: reduce a bit of the boilerplate introduced by Java for some object constructors, getters and setters

# Decisions made

## Security

Bearer token. XSS protection. 

No weird string concatenations on sql queries, in fact only using hibernate naming conventions for queries.


## For database

**TLDR;** POSTGRES inside a container locally. Two DB users for security, one as admin, one as app user.

The project uses a database manager inside a container for development purposes only, this makes project start-up easier
and avoids having to configure it for new developers.
 
For deployment (pre & production) I recommend using a third-party database, 
like the ones provided by RDS AWS, this way we'll have more tools for easy scaling, better performance also they
are cost-efficent relational databases.

The first idea I had was picking Oracle as DB Manager for reliability, support and stability but it
required a paid licence, so as a second option I picked Postgres, which is open source, stable, and has a
big community. I didn't take in count non-relational databases  because there was no real issue
in modeling the application persistence requirements with the plain SQL paradigm.

I'm using two users: AGILE_MONKEY_OWN and AGILE_MONKEY_USR for security reasons. The first one will
be the administration user and owner of all schemas/tables, while the later can only perform 
DELETE, UPDATE and SELECT operations. The AGILE_MONKEY_USR user will be used by the REST API application.

Final steps: change AGILE_MONKEY_OWN and AGILE_MONKEY_USR passwords.

I used pgADMIN as a tool for managing the DB to make administration easier.
